const init = {};

const appReducer = (state = init, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default appReducer;
